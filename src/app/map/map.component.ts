import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  latitude: number;
  longitude: number;

  constructor() { }

  ngOnInit() {
    this.latitude = 51.678418;
    this.longitude = 7.809007;
  }

}
