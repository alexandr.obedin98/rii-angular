import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatExpansionModule} from '@angular/material/expansion';
import {
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatInputModule
} from '@angular/material';

import { CarouselModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { PanelComponent } from './panel/panel.component';
import { TableComponent } from './table/table.component';


@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    PanelComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatCardModule,
    MatButtonModule,
    CarouselModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCdHii2hShICNlRE4Mmj8l64QNzQjJGqfU'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }

